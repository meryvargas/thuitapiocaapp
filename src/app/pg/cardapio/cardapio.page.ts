import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { UrlService } from '../../servidor/url.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-cardapio',
  templateUrl: './cardapio.page.html',
  styleUrls: ['./cardapio.page.scss']
})
export class CardapioPage implements OnInit {
  cardapios: any;

  constructor(public http: Http, public servidorUrl: UrlService) {
    //para verificar se o que ta sendo carregado sera exibido }
    this.listacardapios();
  }
  listacardapios() {
    this.http
      .get(this.servidorUrl.pegarUrl() + 'lista-cardapio.php')
      .pipe(map(res => res.json()))
      .subscribe(listaDados => {
        this.cardapios = listaDados;
        console.log(this.cardapios);
      });
  }
  ngOnInit() {}
}
