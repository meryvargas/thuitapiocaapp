import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular/';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  url: string = 'http://localhost:8080/thuitapioca/admin/';

  constructor(public aviso: AlertController) {}
  pegarUrl() {
    return this.url;
  }
  async alertas(titulo, msg) {
    const alerta = await this.aviso.create({
      header: titulo,
      message: msg,
      buttons: ['Ok']
    });

    await alerta.present();
  }
}
