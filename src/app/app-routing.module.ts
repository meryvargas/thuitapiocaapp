import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'home', loadChildren: './pg/home/home.module#HomePageModule' },
  { path: 'cardapio', loadChildren: './pg/cardapio/cardapio.module#CardapioPageModule' },
  { path: 'contato', loadChildren: './pg/contato/contato.module#ContatoPageModule' },
  { path: 'login', loadChildren: './pg/login/login.module#LoginPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
