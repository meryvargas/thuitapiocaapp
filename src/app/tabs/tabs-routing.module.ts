import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs/pg',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () => import('../pg/home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'login',
        children: [
          {
            path: '',
            loadChildren: () => import('../pg/login/login.module').then(m => m.LoginPageModule)
          }
        ]
      },
      {
        path: 'cardapio',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pg/cardapio/cardapio.module').then(m => m.CardapioPageModule)
          }
        ]
      },
      {
        path: 'contato',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pg/contato/contato.module').then(m => m.ContatoPageModule)
          }
        ]
      },

      {
        path: '',
        redirectTo: '/tabs/pg/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/pg/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
