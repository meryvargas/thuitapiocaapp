﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace ThuiTapiocaDesktop
{
    /// <summary>
    /// Lógica interna para FrmCadCliente.xaml
    /// </summary>
    public partial class FrmCadCliente : Window
    {
		string codigo;

        Storyboard MenuAbrir = new Storyboard();
        Storyboard MenuFechar = new Storyboard();
		//animacoes da linha das caixas//
		
        Storyboard animaNome = new Storyboard();
        Storyboard animaCpf = new Storyboard();
        Storyboard animaTelefone = new Storyboard();
        Storyboard animaCelular = new Storyboard();
        Storyboard animaEmail = new Storyboard();
        Storyboard animaEndereco = new Storyboard();
        Storyboard animaUf = new Storyboard();
        Storyboard animaCep = new Storyboard();
        Storyboard animaLogin = new Storyboard();
        Storyboard animaSenha = new Storyboard();
		//animações das labels do formulario//
		Storyboard animaLblEmpresa = new Storyboard();
		Storyboard animaLblNome = new Storyboard();
		Storyboard animaLblCpf = new Storyboard();
		Storyboard animaLblTelefone = new Storyboard();
		Storyboard animaLblCelular = new Storyboard();
		Storyboard animaLblEmail = new Storyboard();
		Storyboard animaLblEndereco = new Storyboard();
		Storyboard animaLblUf = new Storyboard();
		Storyboard animaLblCep = new Storyboard();
		Storyboard animaLblLogin = new Storyboard();
		Storyboard animaLblSenha = new Storyboard();

		public FrmCadCliente()
        {
            InitializeComponent();
        }

        private void BtnAbrirMenu_Click(object sender, RoutedEventArgs e)
        {
            MenuAbrir = this.FindResource("animacaoMenuAbrir") as Storyboard;
            MenuAbrir.Begin();
        }

        private void BtnFecharMenu_Click(object sender, RoutedEventArgs e)
        {//Precisa arrumar******
            MenuFechar = this.FindResource("animacaoFechaMenu") as Storyboard;
            MenuFechar.Begin();//<-erro aqui, conferir animações no xaml
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            FrmMenuPrincipal frm = new FrmMenuPrincipal();
            frm.Show();
            this.Close();
        }       

        private void BtnNovo_Click(object sender, RoutedEventArgs e)
		{//Cmb Empresa não tem contorno como os demais textBox

			CmbEmpresa.IsEnabled = true;
			CmbEmpresa.Focus();

            BtnLimpar.IsEnabled = true;
            BtnNovo.IsEnabled = false;
			//animação do Lbl empresa>Subir< 
			animaLblEmpresa = this.FindResource("animacaoEmpresa") as Storyboard;

            animaLblEmpresa.AutoReverse = false;
            animaLblEmpresa.Begin(this, false);
            animaLblEmpresa.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            CmbEmpresa.SelectedIndex = -1;
			TxtNome.Clear();
            TxtCpf.Clear();
            TxtTelefone.Clear();
            TxtCelular.Clear();
            TxtEmail.Clear();
            TxtEndereco.Clear();
            TxtUf.Clear();
            TxtCep.Clear();
            TxtLogin.Clear();
            TxtSenha.Clear();
			TxtDataCadCliente.Clear();
			ChkStatusCli.IsChecked = false;

			//Começo da animação das linhas das caixas
			//Txt animações de contorno das mesmas no Btn Limpar para desabilitar ao clicar o botao

			animaNome.AutoReverse = true;
			animaNome.Begin(this, true);
			animaNome.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

			animaCpf.AutoReverse = true;
			animaCpf.Begin(this, true);
			animaCpf.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

			animaTelefone.AutoReverse = true;
			animaTelefone.Begin(this, true);
			animaTelefone.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

			animaCelular.AutoReverse = true;
			animaCelular.Begin(this, true);
			animaCelular.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

			animaEmail.AutoReverse = true;
			animaEmail.Begin(this, true);
			animaEmail.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

			animaEndereco.AutoReverse = true;
			animaEndereco.Begin(this, true);
			animaEndereco.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

			animaUf.AutoReverse = true;
			animaUf.Begin(this, true);
			animaUf.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

			animaCep.AutoReverse = true;
			animaCep.Begin(this, true);
			animaCep.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

			animaLogin.AutoReverse = true;
			animaLogin.Begin(this, true);
			animaLogin.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

			animaSenha.AutoReverse = true;
			animaSenha.Begin(this, true);
			animaSenha.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);
			//FIM da animacao 1


			//animacao dos titulos de cada campo(labels)
			//animações de lbl das mesmas no Btn Limpar para desabilitar ao clicar o botao

			animaLblEmpresa.AutoReverse = true;
            animaLblEmpresa.Begin(this, true);
            animaLblEmpresa.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblNome.AutoReverse = true;
            animaLblNome.Begin (this, true);
            animaLblNome.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblCpf.AutoReverse = true;
            animaLblCpf.Begin(this, true);
            animaLblCpf.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblTelefone.AutoReverse = true;
            animaLblTelefone.Begin(this, true);
            animaLblTelefone.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblCelular.AutoReverse = true;
            animaLblCelular.Begin(this, true);
            animaLblCelular.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblEmail.AutoReverse = true;
            animaLblEmail.Begin(this, true);
            animaLblEmail.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblEndereco.AutoReverse = true;
            animaLblEndereco.Begin(this, true);
            animaLblEndereco.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblUf.AutoReverse = true;
            animaLblUf.Begin(this, true);
            animaLblUf.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblCep.AutoReverse = true;
            animaLblCep.Begin(this, true);
            animaLblCep.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblLogin.AutoReverse = true;
            animaLblLogin.Begin(this, true);
            animaLblLogin.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            animaLblSenha.AutoReverse = true;
            animaLblSenha.Begin(this, true);
            animaLblSenha.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);

            //fim da animacao 2

            CmbEmpresa.IsEnabled = false;
            TxtNome.IsEnabled = false;
            TxtCpf.IsEnabled = false;
            TxtTelefone.IsEnabled = false;
            TxtCelular.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            TxtEndereco.IsEnabled = false;
            TxtUf.IsEnabled = false;
            TxtCep.IsEnabled = false;
            TxtLogin.IsEnabled = false;
            TxtSenha.IsEnabled = false;
			TxtDataCadCliente.IsEnabled = false;
			ChkStatusCli.IsEnabled = false;

            BtnLimpar.IsEnabled = false;
            BtnSalvar.IsEnabled = false;

            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();
            
        }

        private void CmbEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            TxtNome.IsEnabled = true;
            TxtNome.Focus();

            animaLblNome = this.FindResource("animacaoNome") as Storyboard;

            animaLblNome.AutoReverse = false;
            animaLblNome.Begin(this, false);
            animaLblNome.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

            if (e.Key == Key.Enter)
            {
                Banco bd = new Banco();
                bd.Conectar();
                MySqlCommand comm = new MySqlCommand("SELECT * FROM empresa WHERE nome = ?", bd.conexao);
                comm.Parameters.Clear();
                comm.Parameters.Add("@nome", MySqlDbType.String).Value = CmbEmpresa.Text;

                comm.CommandType = CommandType.Text;

                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                codigo = dr.GetString(0);

            }
        }

        private void TxtNome_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }


            if (e.Key == Key.Enter)
            {
				//Animação da txtBox nome>contorno<
				animaNome = this.FindResource("animacaoTxtNome") as Storyboard;
				animaNome.AutoReverse = false;
				animaNome.Begin(this, false);
				animaNome.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);
				//Animação da Labels Cpf>subir<>Labels<
				//a animação da label de nome está habilitada para começar depois que a CmbEmpresa ser clicada>enter<
				animaLblCpf = this.FindResource("animacaoCpf") as Storyboard;

                animaLblCpf.AutoReverse = false;
                animaLblCpf.Begin(this, false);
                animaLblCpf.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

                TxtCpf.IsEnabled = true;
                TxtCpf.Focus();
            }
        }

        private void TxtCpf_KeyDown(object sender, KeyEventArgs e)
        {
			TxtCpf.MaxLength = 14;

			KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)&&(e.Key != Key.OemPeriod)
				&&(e.Key != Key.OemMinus)&&(e.Key != Key.Enter)&&(e.Key != Key.LeftShift)&&(e.Key != Key.Tab) && (e.Key != Key.NumLock) &&(e.Key!= Key.NumPad0)&& (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
				&&(e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9) &&(e.Key != Key.Subtract))

			{
                MessageBox.Show("Este campo somente aceita números, '.' e '-' ! ");

                e.Handled = true;
            }//somente numeros e os especiais necessarios//

            if (e.Key == Key.Enter)
            {
				animaCpf = this.FindResource("animacaoTxtCpf") as Storyboard;
				animaCpf.AutoReverse = false;
				animaCpf.Begin(this, false);
				animaCpf.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

				animaLblTelefone = this.FindResource("animacaoTelefone") as Storyboard;

				animaLblTelefone.AutoReverse = false;
				animaLblTelefone.Begin(this, false);
				animaLblTelefone.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

				TxtTelefone.IsEnabled = true;
                TxtTelefone.Focus();
            }
        }

        private void TxtTelefone_KeyDown(object sender, KeyEventArgs e)
        {

			TxtTelefone.MaxLength = 13;

			KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false) && (e.Key != Key.OemMinus) 
            && (e.Key != Key.Enter) && (e.Key != Key.LeftShift) && (e.Key != Key.RightShift) && (e.Key != Key.Tab)&& (e.Key != Key.NumLock)&& (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) 
			&& (e.Key != Key.NumPad3) && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9)&&(e.Key!= Key.Subtract))

			{
				MessageBox.Show("Este campo somente aceita números e '-' ! ");

                e.Handled = true;
            }//converter as letras por numeros e declarar os especiais necessario//

            if (e.Key == Key.Enter)
            {

                animaTelefone = this.FindResource("animacaoTxtTelefone") as Storyboard;
				animaTelefone.AutoReverse = false;
				animaTelefone.Begin(this, false);
				animaTelefone.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);


				animaLblCelular = this.FindResource("animacaoCelular") as Storyboard;

                animaLblCelular.AutoReverse = false;
                animaLblCelular.Begin(this, false);
                animaLblCelular.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

                TxtCelular.IsEnabled = true;
                TxtCelular.Focus();
            }
        }

        private void TxtCelular_KeyDown(object sender, KeyEventArgs e)
        {
			TxtCelular.MaxLength = 14;

			KeyConverter key = new KeyConverter();
			if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false) && (e.Key != Key.OemMinus)
			&& (e.Key != Key.Enter) && (e.Key != Key.LeftShift) && (e.Key != Key.RightShift) && (e.Key != Key.Tab) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1)
			&& (e.Key != Key.NumPad3) && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9) && (e.Key!= Key.Subtract))
			{
                MessageBox.Show("Este campo somente aceita números e '-' ! ");

                e.Handled = true;
            }//converter as letras por numeros e declarar os especiais necessario//

            if (e.Key == Key.Enter)
            {
                animaCelular = this.FindResource("animacaoTxtCelular") as Storyboard;
				animaCelular.AutoReverse = false;
				animaCelular.Begin(this, false);
				animaCelular.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

				animaLblEmail = this.FindResource("animacaoEmail") as Storyboard;

                animaLblEmail.AutoReverse = false;
                animaLblEmail.Begin(this, false);
                animaLblEmail.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);
                
                TxtEmail.IsEnabled = true;
                TxtEmail.Focus();
            }
        }

        private void TxtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                animaEmail = this.FindResource("animacaoTxtEmail") as Storyboard;
				animaEmail.AutoReverse = false;
				animaEmail.Begin(this, false);
				animaEmail.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

				animaLblEndereco = this.FindResource("animacaoEndereco") as Storyboard;

                animaLblEndereco.AutoReverse = false;
                animaLblEndereco.Begin(this, false);
                animaLblEndereco.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

                TxtEndereco.IsEnabled = true;
                TxtEndereco.Focus();
            }
        }

        private void TxtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
			KeyConverter key = new KeyConverter();
			if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
			{
				MessageBox.Show("Esse campo somente aceita letras de A - Z");

				e.Handled = true;
			}

			if (e.Key == Key.Enter)
            {
                animaEndereco = this.FindResource("animacaoTxtEndereco") as Storyboard;
				animaEndereco.AutoReverse = false;
				animaEndereco.Begin(this, false);
				animaEndereco.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

				animaLblUf = this.FindResource("animacaoUf") as Storyboard;

                animaLblUf.AutoReverse = false;
                animaLblUf.Begin(this, false);
                animaLblUf.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

                TxtUf.IsEnabled = true;
                TxtUf.Focus();
            }
        }

        private void TxtUf_KeyDown(object sender, KeyEventArgs e)
        {
			KeyConverter key = new KeyConverter();
			if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
			{
				MessageBox.Show("Esse campo somente aceita letras de A - Z, referentes a unidade de federação.");

				e.Handled = true;
			}

			TxtUf.MaxLength = 2;

			if (e.Key == Key.Enter)
            {
                animaUf = this.FindResource("animacaoTxtUf") as Storyboard;
				animaUf.AutoReverse = false;
				animaUf.Begin(this, false);
				animaUf.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

				animaLblCep = this.FindResource("animacaoCep") as Storyboard;

                animaLblCep.AutoReverse = false;
                animaLblCep.Begin(this, false);
                animaLblCep.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

                TxtCep.IsEnabled = true;
                TxtCep.Focus();
            }
        }

        private void TxtCep_KeyDown(object sender, KeyEventArgs e)
        {
			TxtCep.MaxLength = 9;

			KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false) && (e.Key != Key.OemMinus)
            && (e.Key != Key.Enter) && (e.Key != Key.LeftShift) && (e.Key != Key.RightShift) && (e.Key != Key.Tab) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1)
			&& (e.Key != Key.NumPad3) && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))
            {
                MessageBox.Show("Este campo somente aceita números e '-' ! ");

                e.Handled = true;
            }
            //converter as letras por numeros e declarar os especiais necessario//

            if (e.Key == Key.Enter)
            {
				animaCep = this.FindResource("animacaoTxtCep") as Storyboard;
				animaCep.AutoReverse = false;
				animaCep.Begin(this, false);
				animaCep.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

				animaLblLogin = this.FindResource("animacaoLogin") as Storyboard;

                animaLblLogin.AutoReverse = false;
                animaLblLogin.Begin(this, false);
                animaLblLogin.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

                TxtLogin.IsEnabled = true;
                TxtLogin.Focus();
            }
        }

        private void TxtLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                animaLogin = this.FindResource("animacaoTxtLogin") as Storyboard;
				animaLogin.AutoReverse = false;
				animaLogin.Begin(this, false);
				animaLogin.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

				animaLblSenha = this.FindResource("animacaoSenha") as Storyboard;

                animaLblSenha.AutoReverse = false;
                animaLblSenha.Begin(this, false);
                animaLblSenha.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);

                TxtSenha.IsEnabled = true;
                TxtSenha.Focus();
            }
        }

        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
				TxtDataCadCliente.Text = DateTime.Now.ToShortDateString();
				ChkStatusCli.IsEnabled = true;
				MessageBox.Show("Não esqueça do status do cliente!");

                animaSenha = this.FindResource("animacaoTxtSenha") as Storyboard;
				animaSenha.AutoReverse = false;
				animaSenha.Begin(this, false);
				animaSenha.Seek(this, new TimeSpan(0, 0, 3), TimeSeekOrigin.Duration);


				BtnSalvar.IsEnabled = true;
            }
        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime dataCadastro = DateTime.Today;
			
			Banco bd = new Banco();
			bd.Conectar();
			if (ChkStatusCli.IsChecked == true)
			{
				status = "ATIVO";

				string inserir = "INSERT INTO cliente(idEmpresa,nome,cpf,telefone,celular,email,endereco,uf,cep,login,senha,dataCadCli,statusCli)VALUES('" 
					+ codigo + "','"
					+ TxtNome.Text + "','"
					+ TxtCpf.Text + "','"
					+ TxtTelefone.Text + "','"
					+ TxtCelular.Text + "','"
					+ TxtEmail.Text + "','"
					+ TxtEndereco.Text + "','"
					+ TxtUf.Text + "','"
					+ TxtCep.Text + "','"
					+ TxtLogin.Text + "','"
					+ TxtSenha.Text + "','"
					+ dataCadastro.ToString("yyyy-MM-dd") + "','"
					+ status + "')";

				MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
				comandos.ExecuteNonQuery();
			}

			else
			{
				status = "INATIVO";

				string inserir = "INSERT INTO cliente(idEmpresa,nome,cpf,telefone,celular,email,endereco,uf,cep,login,senha,dataCadCli,statusCli)VALUES('" 
					+ codigo + "','"
					+ TxtNome.Text + "','"
					+ TxtCpf.Text + "','"
					+ TxtTelefone.Text + "','"
					+ TxtCelular.Text + "','"
					+ TxtEmail.Text + "','"
					+ TxtEndereco.Text + "','"
					+ TxtUf.Text + "','"
					+ TxtCep.Text + "','"
					+ TxtLogin.Text + "','"
					+ TxtSenha.Text + "','"
					+ dataCadastro.ToString("yyyy-MM-dd") + "','"
					+ status + "')";

				MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
				comandos.ExecuteNonQuery();
			}

            bd.Desconectar();
            MessageBox.Show("Cliente cadastrado com sucesso!!", "Cadastro de Cliente");
            BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            
        }

        private void FrmCadCliente1_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idEmpresa, nome FROM empresa";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CmbEmpresa.DisplayMemberPath = "nome";
            CmbEmpresa.ItemsSource = dt.DefaultView;

            
        }

        private void BtnTodosClientes_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            FrmTodosClientes frm = new FrmTodosClientes();
            frm.Show();
            this.Close();
        }
    }
}
