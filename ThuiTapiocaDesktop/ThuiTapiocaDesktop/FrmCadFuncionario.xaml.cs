﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThuiTapiocaDesktop
{
    /// <summary>
    /// Lógica interna para FrmCadFuncionario.xaml
    /// </summary>
    public partial class FrmCadFuncionario : Window
    {
        string codigoEmp ;
        public FrmCadFuncionario()
        {
            InitializeComponent();
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            FrmMenuPrincipal frm = new FrmMenuPrincipal();
            frm.Show();
            this.Close();
        }

        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            //Btn novo apertado> txtNome habilitado.
            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = false;

            CmbEmpresa.IsEnabled = true;
            CmbEmpresa.Focus();
        }
        private void CmbEmpresa_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idEmpresa, nome FROM empresa";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CmbEmpresa.DisplayMemberPath = "nome";
            CmbEmpresa.ItemsSource = dt.DefaultView;

        }

        private void CmbEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Banco bd = new Banco();
                bd.Conectar();
                MySqlCommand comm = new MySqlCommand("SELECT * FROM empresa WHERE nome = ?", bd.conexao);
                comm.Parameters.Clear();
                comm.Parameters.Add("@nome", MySqlDbType.String).Value = CmbEmpresa.Text;

                comm.CommandType = CommandType.Text;

                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                codigoEmp = dr.GetString(0);
            }
            if (e.Key == Key.Enter)
            {
                TxtNome.IsEnabled = true;
                TxtNome.Focus();

            }
        }

        private void TxtNome_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                BtnLimpar.IsEnabled = true;//SENDO habilitado novamente

                TxtEndereco.IsEnabled = true;
                TxtEndereco.Focus();
            }
        }

        private void TxtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtNumero.IsEnabled = true;
                TxtNumero.Focus();
            }
        }

        private void TxtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e pontuação se necessária!");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtComplemento.IsEnabled = true;
                TxtComplemento.Focus();
            }
        }

        private void TxtComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtBairro.IsEnabled = true;
                TxtBairro.Focus();
            }
        }

        private void TxtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtCidade.IsEnabled = true;
                TxtCidade.Focus();
            }
        }
        private void TxtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtUf.IsEnabled = true;
                TxtUf.Focus();
            }
        }

        private void TxtUf_KeyDown(object sender, KeyEventArgs e)
        {
            TxtUf.MaxLength = 2;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z, referentes a unidade de federação.");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtCep.IsEnabled = true;
                TxtCep.Focus();
            }
        }

        private void TxtCep_KeyDown(object sender, KeyEventArgs e)
        {
            TxtCep.MaxLength = 9;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e '-' ");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtEmail.IsEnabled = true;
                TxtEmail.Focus();
            }
        }

        private void TxtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            TxtTelefone.MaxLength = 13;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e pontuação necessária!");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtCelular.IsEnabled = true;
                TxtCelular.Focus();
            }
        }

        private void TxtCelular_KeyDown(object sender, KeyEventArgs e)
        {
            TxtTelefone.MaxLength = 14;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e pontuação necessária!");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtEmail.IsEnabled = true;
                TxtEmail.Focus();
            }
        }

        private void TxtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtDataAdmissao.IsEnabled = true;
                TxtDataAdmissao.Focus();
            }

        }

        private void TxtDataAdmissao_KeyDown(object sender, KeyEventArgs e)
        {
            TxtDataAdmissao.MaxHeight = 10;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e pontuação necessária!");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtDataDemissao.IsEnabled = true;
                TxtDataDemissao.Focus();
            }
        }

        private void TxtDataDemissao_KeyDown(object sender, KeyEventArgs e)
        {
            TxtDataDemissao.MaxHeight = 10;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e pontuação necessária!");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtCargo.IsEnabled = true;
                TxtCargo.Focus();
            }
        }

        private void TxtCargo_KeyDown(object sender, KeyEventArgs e)
        {//Passará por alterações, talvez essa campo mude para um radiobutton ou combobox
            if (e.Key == Key.Enter)
            {

                TxtSenha.IsEnabled = true;
                TxtSenha.Focus();
            }
        }

        //private void TxtLogin_KeyDown(object sender, KeyEventArgs e)
        //{

        //}

        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ChkStatusFuncionario.IsEnabled = true;
                MessageBox.Show("Não esqueça do status do funcionario!S");
                TxtDataCadFuncionario.Text = DateTime.Now.ToShortDateString();

                BtnSalvar.IsEnabled = true;
                BtnSalvar.Focus();
            }
        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime DataCadastro = DateTime.Today;

            Banco bd = new Banco();
            bd.Conectar();
            if (ChkStatusFuncionario.IsChecked == true)
            {
                status = "ATIVO";

                string inserir = "INSERT INTO funcionario(idEmpresa,nome,endereco,numero,complemento,bairro,cidade,uf,cep,telefone,celular,email,cargo,dataAdmissao,dataDemissao,senha,statusFuncionario,dataCadFuncionario)VALUES('"
                 + codigoEmp + "','"
                 + TxtNome.Text + "','"
                 + TxtEndereco.Text + "','"
                 + TxtNumero.Text + "','"
                 + TxtComplemento.Text + "','"
                 + TxtBairro.Text + "','"
                 + TxtCidade.Text + "','"
                 + TxtUf.Text + "','"
                 + TxtCep.Text + "','"
                 + TxtTelefone.Text + "','"
                 + TxtCelular.Text + "','"
                 + TxtEmail.Text + "','"
                 + TxtCargo.Text + "','"
                 + TxtDataAdmissao.Text + "','"
                 + TxtDataDemissao.Text + "','"
                 + TxtSenha.Text + "','"
                 + status + "','"
                 + DataCadastro.ToString("yyyy-MM--dd") + "')";


                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            else
            {
                status = "INATIVO";

                string inserir = "INSERT INTO funcionario(idEmpresa,nome,endereco,numero,complemento,bairro,cidade,uf,cep,telefone,celular,email,cargo,dataAdmissao,dataDemissao,senha,statusFuncionario,dataCadFuncionario)VALUES('"
                  + codigoEmp + "','"
                  + TxtNome.Text + "','"
                  + TxtEndereco.Text + "','"
                  + TxtNumero.Text + "','"
                  + TxtComplemento.Text + "','"
                  + TxtBairro.Text + "','"
                  + TxtCidade.Text + "','"
                  + TxtUf.Text + "','"
                  + TxtCep.Text + "','"
                  + TxtTelefone.Text + "','"
                  + TxtCelular.Text + "','"
                  + TxtEmail.Text + "','"
                  + TxtCargo.Text + "','"
                  + TxtDataAdmissao.Text + "','"
                  + TxtDataDemissao.Text + "','"
                  + TxtSenha.Text + "','"
                  + status + "','"
                  + DataCadastro.ToString("yyyy-MM--dd") + "')";

                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }

            bd.Desconectar();
            MessageBox.Show("Fornecedor cadastrado com sucesso!!", "Cadastro de fornecedores");

            //desabilitando os campos
            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            BtnSalvar.IsEnabled = false;

            TxtNome.IsEnabled = false;
            TxtEndereco.IsEnabled = false;
            TxtNumero.IsEnabled = false;
            TxtComplemento.IsEnabled = false;
            TxtBairro.IsEnabled = false;
            TxtCidade.IsEnabled = true;
            TxtUf.IsEnabled = false;
            TxtCep.IsEnabled = false;
            TxtTelefone.IsEnabled = false;
            TxtCelular.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            TxtCargo.IsEnabled = false;
            TxtDataAdmissao.IsEnabled = false;
            TxtDataDemissao.IsEnabled = false;
            TxtSenha.IsEnabled = false;
            TxtDataCadFuncionario.IsEnabled = false;
            ChkStatusFuncionario.IsEnabled = false;
            //SEM essas linhas de clear os campos preenchidos permanecem preenchidos mesmo após salvar
            TxtNome.Clear();
            TxtEndereco.Clear();
            TxtNumero.Clear();
            TxtComplemento.Clear();
            TxtBairro.Clear();
            TxtCidade.Clear();
            TxtUf.Clear();
            TxtCep.Clear();
            TxtTelefone.Clear();
            TxtCelular.Clear();
            TxtEmail.Clear();
            TxtCargo.Clear();
            TxtDataAdmissao.Clear();
            TxtDataDemissao.Clear();
            TxtSenha.Clear();
            TxtDataCadFuncionario.Clear();
            ChkStatusFuncionario.IsChecked = false;
        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            //limpar
            TxtNome.Clear();
            TxtEndereco.Clear();
            TxtNumero.Clear();
            TxtComplemento.Clear();
            TxtBairro.Clear();
            TxtCidade.Clear();
            TxtUf.Clear();
            TxtCep.Clear();
            TxtTelefone.Clear();
            TxtCelular.Clear();
            TxtEmail.Clear();
            TxtCargo.Clear();
            TxtDataAdmissao.Clear();
            TxtDataDemissao.Clear();
            TxtSenha.Clear();
            TxtDataCadFuncionario.Clear();
            ChkStatusFuncionario.IsChecked = false;
            //FIM DA LIMPEZA
            //desabilitando os botões
            TxtNome.IsEnabled = false;
            TxtEndereco.IsEnabled = false;
            TxtNumero.IsEnabled = false;
            TxtComplemento.IsEnabled = false;
            TxtBairro.IsEnabled = false;
            TxtCidade.IsEnabled = true;
            TxtUf.IsEnabled = false;
            TxtCep.IsEnabled = false;
            TxtTelefone.IsEnabled = false;
            TxtCelular.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            TxtCargo.IsEnabled = false;
            TxtDataAdmissao.IsEnabled = false;
            TxtDataDemissao.IsEnabled = false;
            TxtSenha.IsEnabled = false;
            TxtDataCadFuncionario.IsEnabled = false;
            ChkStatusFuncionario.IsEnabled = false;

            BtnSalvar.IsEnabled = false;
            BtnLimpar.IsEnabled = false;//desabilitado para voltar somente dps de se aperta NOVO e enter no primeiro item>NOME<
            //Unico botão ativo.
            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();
        }

        
    }
}
