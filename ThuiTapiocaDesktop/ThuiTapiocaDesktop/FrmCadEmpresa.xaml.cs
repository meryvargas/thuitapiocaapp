﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThuiTapiocaDesktop
{
    /// <summary>
    /// Lógica interna para FrmCadEmpresa.xaml
    /// </summary>
    public partial class FrmCadEmpresa : Window
    {
        public FrmCadEmpresa()
        {
            InitializeComponent();
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            FrmMenuPrincipal frm = new FrmMenuPrincipal();
            frm.Show();
            this.Close();
        }

        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = false;

            TxtNome.IsEnabled = true;
            TxtNome.Focus();
        }

        private void TxtNome_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                BtnLimpar.IsEnabled = true;//SENDO habilitado novamente

                TxtEndereco.IsEnabled = true;
                TxtEndereco.Focus();
            }
        }

        private void TxtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtNumero.IsEnabled = true;
                TxtNumero.Focus();
            }
        }

        private void TxtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e pontuação se necessária!");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtComplemento.IsEnabled = true;
                TxtComplemento.Focus();
            }
        }

        private void TxtComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtBairro.IsEnabled = true;
                TxtBairro.Focus();
            }
        }

        private void TxtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtCidade.IsEnabled = true;
                TxtCidade.Focus();
            }
        }

        private void TxtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtUf.IsEnabled = true;
                TxtUf.Focus();
            }
        }

        private void TxtUf_KeyDown(object sender, KeyEventArgs e)
        {
            TxtUf.MaxLength = 2;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == true))
            {
                MessageBox.Show("Esse campo somente aceita letras de A - Z, referentes a unidade de federação.");

                e.Handled = true;
            }
            if (e.Key == Key.Enter)
            {
                TxtCep.IsEnabled = true;
                TxtCep.Focus();
            }
        }

        private void TxtCep_KeyDown(object sender, KeyEventArgs e)
        {
            TxtCep.MaxLength = 9;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e '-' ");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtEmail.IsEnabled = true;
                TxtEmail.Focus();
            }
        }

        private void TxtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtTelefone1.IsEnabled = true;
                TxtTelefone1.Focus();
            }

        }

        private void TxtTelefone1_KeyDown(object sender, KeyEventArgs e)
        {
            TxtTelefone1.MaxLength = 13;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e pontuação necessária!");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtTelefone2.IsEnabled = true;
                TxtTelefone2.Focus();
            }
        }

        private void TxtTelefone2_KeyDown(object sender, KeyEventArgs e)
        {
            TxtTelefone2.MaxLength = 13;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números e pontuação necessária!");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {

                TxtDataAbertura.IsEnabled = true;
                TxtDataAbertura.Focus();
            }
        }

        private void TxtDataAbertura_KeyDown(object sender, KeyEventArgs e)
        {//Essa parte pode sofrer alterações para algo mais preciso(como colocar um calendario..)
            TxtDataAbertura.MaxLength = 10;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números referentes a data e '-' ");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {
                TxtDataCad.Text = DateTime.Now.ToShortDateString();

                TxtDataFim.IsEnabled = true;
                TxtDataFim.Focus();
            }
        }

        private void TxtDataFim_KeyDown(object sender, KeyEventArgs e)
        {
            TxtDataFim.MaxLength = 10;
            KeyConverter key = new KeyConverter();
            if ((char.IsNumber((string)key.ConvertTo(e.Key, typeof(string)), 0) == false)
                && (e.Key != Key.OemMinus) && (e.Key != Key.Enter) && (e.Key != Key.NumLock) && (e.Key != Key.NumPad0) && (e.Key != Key.NumPad1) && (e.Key != Key.NumPad3)
                && (e.Key != Key.NumPad4) && (e.Key != Key.NumPad5) && (e.Key != Key.NumPad6) && (e.Key != Key.NumPad7) && (e.Key != Key.NumPad8) && (e.Key != Key.NumPad9))

            {
                MessageBox.Show("Este campo somente aceita números referentes a data e '-' ");

                e.Handled = true;
            }//somente numeros e pontuação 

            if (e.Key == Key.Enter)
            {
                BtnLimpar.IsEnabled = true;

                BtnSalvar.IsEnabled = true;
                BtnSalvar.Focus();
            }
        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            //limpar
            TxtNome.Clear();
            TxtEndereco.Clear();
            TxtNumero.Clear();
            TxtComplemento.Clear();
            TxtBairro.Clear();
            TxtCidade.Clear();
            TxtUf.Clear();
            TxtCep.Clear();
            TxtTelefone1.Clear();
            TxtTelefone2.Clear();
            TxtEmail.Clear();
            TxtDataAbertura.Clear();
            TxtDataFim.Clear();
            TxtDataCad.Clear();
            //FIM DA LIMPEZA
            //desabilitando os botões
            TxtNome.IsEnabled = false;
            TxtEndereco.IsEnabled = false;
            TxtNumero.IsEnabled = false;
            TxtComplemento.IsEnabled = false;
            TxtBairro.IsEnabled = false;
            TxtCidade.IsEnabled = false;
            TxtUf.IsEnabled = false;
            TxtCep.IsEnabled = false;
            TxtTelefone1.IsEnabled = false;
            TxtTelefone2.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            TxtDataAbertura.IsEnabled = false;
            TxtDataFim.IsEnabled = false;
            TxtDataCad.IsEnabled = false;

            BtnLimpar.IsEnabled = false;//desabilitado para voltar somente dps de se aperta NOVO e enter no primeiro item>NOME<
            //Unico botão ativo.
            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();
        }

    private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {

            Banco bd = new Banco();
            bd.Conectar();
            DateTime DataCadastro = DateTime.Today;

            string inserir = "INSERT INTO empresa(nome,endereco,numero,complemento,bairro,cidade,uf,cep,telefone1,telefone2,email,dataAbertura,dataFim,dataCadEmpresa)VALUES('"
                + TxtNome.Text + "','"
                + TxtEndereco.Text + "','"
                + TxtNumero.Text + "','"
                + TxtComplemento.Text + "','"
                + TxtBairro.Text + "','"
                + TxtCidade.Text + "','"
                + TxtUf.Text + "','"
                + TxtCep.Text + "','"
                + TxtTelefone1.Text + "','"
                + TxtTelefone2.Text + "','"
                + TxtEmail.Text + "','"
                + TxtDataAbertura.Text + "','"
               + TxtDataFim.Text + "','"
                + DataCadastro.ToString("yyyy-MM--dd") + "')";

            MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
            comandos.ExecuteNonQuery();

            bd.Desconectar();

            MessageBox.Show("Empresa cadastrada com sucesso!!", "Cadastro de Empresa");
            BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            //desbilitando os campos
            TxtNome.IsEnabled = false;
            TxtEndereco.IsEnabled = false;
            TxtNumero.IsEnabled = false;
            TxtComplemento.IsEnabled = false;
            TxtBairro.IsEnabled = false;
            TxtCidade.IsEnabled = false;
            TxtUf.IsEnabled = false;
            TxtCep.IsEnabled = false;
            TxtTelefone1.IsEnabled = false;
            TxtTelefone2.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            TxtDataAbertura.IsEnabled = false;
            TxtDataFim.IsEnabled = false;
            TxtDataCad.IsEnabled = false;
            //desabilitando e habilitando o necessario
            BtnSalvar.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            //retirando conteudo dos campos
            TxtNome.Clear();
            TxtEndereco.Clear();
            TxtNumero.Clear();
            TxtComplemento.Clear();
            TxtBairro.Clear();
            TxtCidade.Clear();
            TxtUf.Clear();
            TxtCep.Clear();
            TxtTelefone1.Clear();
            TxtTelefone2.Clear();
            TxtEmail.Clear();
            TxtDataAbertura.Clear();
            TxtDataFim.Clear();
            TxtDataCad.Clear();

        }

    }
}
