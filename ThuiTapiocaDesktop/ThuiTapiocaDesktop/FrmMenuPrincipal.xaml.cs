﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThuiTapiocaDesktop
{
    /// <summary>
    /// Lógica interna para FrmMenuPrincipal.xaml
    /// </summary>
    public partial class FrmMenuPrincipal : Window
    {
        Storyboard anima1 = new Storyboard();
        Storyboard anima2 = new Storyboard();


        public FrmMenuPrincipal()
        {
             InitializeComponent();

            anima1.AutoReverse = true;
            anima1.RepeatBehavior = RepeatBehavior.Forever;
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
		// Btn para outras paginas

		private void BtnFuncionario_MouseDown(object sender, MouseButtonEventArgs e)
		{
			Hide();
			FrmCadFuncionario frm = new FrmCadFuncionario();
			frm.Show();

		}

		private void BtnCliente_MouseDown(object sender, MouseButtonEventArgs e)
		{
			Hide();
			FrmCadCliente frm = new FrmCadCliente();
			frm.Show();

		}

		//private void BtnPedido_MouseDown(object sender, MouseButtonEventArgs e)
		//{
		//	Hide();
		//	FrmCadPedido frm = new FrmCadPedido();
		//	frm.Show();

		//}

		private void BtnCardapio_MouseDown (object sender, MouseButtonEventArgs e)
		{
			Hide();
			FrmCadCardapio frm = new FrmCadCardapio();
			frm.Show();

		}
		private void BtnEmpresa_MouseDown(object sender, MouseButtonEventArgs e)
		{
			Hide();
			FrmCadEmpresa frm = new FrmCadEmpresa();
			frm.Show();

		}
		private void BtnFornecedor_MouseDown(object sender, MouseButtonEventArgs e)
		{
			Hide();
			FrmCadFornecedor frm = new FrmCadFornecedor();
			frm.Show();

		}
		//Fim dos btn

		//Lbl de animações

		private void LblFuncionario_MouseMove(object sender, MouseEventArgs e)
        {
            anima2 = this.FindResource("animacaoFuncionario") as Storyboard;
            anima2.Begin();
        }

        private void LblCliente_MouseMove(object sender, MouseEventArgs e)
        {
            anima2 = this.FindResource("animacaoCliente") as Storyboard;
            anima2.Begin();

        }

        private void LblPedido_MouseMove(object sender, MouseEventArgs e)
        {
            anima2 = this.FindResource("animacaoPedido") as Storyboard;
            anima2.Begin();
        }

        private void LblCardapio_MouseMove(object sender, MouseEventArgs e)
        {
            anima2 = this.FindResource("animacaoCardapio") as Storyboard;
            anima2.Begin();

        }

        private void LblEmpresa_MouseMove(object sender, MouseEventArgs e)
        {
            anima2 = this.FindResource("animacaoEmpresa") as Storyboard;
            anima2.Begin();
        }

        private void LblFornecedor_MouseMove(object sender, MouseEventArgs e)
        {
            anima2 = this.FindResource("animacaoFornecedor") as Storyboard;
            anima2.Begin();
        }


		//Fim das animações

	}
}
