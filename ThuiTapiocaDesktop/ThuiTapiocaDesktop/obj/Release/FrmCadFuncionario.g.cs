﻿#pragma checksum "..\..\FrmCadFuncionario.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4EDDF714C936614E137AE3AA8EF0DB19"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ThuiTapiocaDesktop;


namespace ThuiTapiocaDesktop {
    
    
    /// <summary>
    /// FrmCadFuncionario
    /// </summary>
    public partial class FrmCadFuncionario : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ThuiTapiocaDesktop.FrmCadFuncionario FrmCadFuncionario1;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCadastroFuncionario;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSalvar;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnNovo;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnLimpar;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSair;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtNome;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtNumero;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtComplemento;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtBairro;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtCidade;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtUf;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtCep;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtDataAdmissao;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtDataDemissao;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtEndereco;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblNome;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblEndereco;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblNumero;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblComplemento;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblBairro;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCidade;
        
        #line default
        #line hidden
        
        
        #line 185 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LbllUf;
        
        #line default
        #line hidden
        
        
        #line 195 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCep;
        
        #line default
        #line hidden
        
        
        #line 205 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblDataAdmissão;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblDataDemissao;
        
        #line default
        #line hidden
        
        
        #line 225 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtEmail;
        
        #line default
        #line hidden
        
        
        #line 233 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblEmail;
        
        #line default
        #line hidden
        
        
        #line 243 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtLogin;
        
        #line default
        #line hidden
        
        
        #line 251 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtSenha;
        
        #line default
        #line hidden
        
        
        #line 259 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblLogin;
        
        #line default
        #line hidden
        
        
        #line 269 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblSenha;
        
        #line default
        #line hidden
        
        
        #line 279 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtCargo;
        
        #line default
        #line hidden
        
        
        #line 287 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtTelefone;
        
        #line default
        #line hidden
        
        
        #line 295 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblTelefone;
        
        #line default
        #line hidden
        
        
        #line 305 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtCelular;
        
        #line default
        #line hidden
        
        
        #line 313 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCelular;
        
        #line default
        #line hidden
        
        
        #line 323 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCargo;
        
        #line default
        #line hidden
        
        
        #line 333 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CmbEmpresa;
        
        #line default
        #line hidden
        
        
        #line 342 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblEmpresa;
        
        #line default
        #line hidden
        
        
        #line 352 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtDataCadCliente;
        
        #line default
        #line hidden
        
        
        #line 360 "..\..\FrmCadFuncionario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblDatCadCliente;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ThuiTapiocaDesktop;component/frmcadfuncionario.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\FrmCadFuncionario.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.FrmCadFuncionario1 = ((ThuiTapiocaDesktop.FrmCadFuncionario)(target));
            return;
            case 2:
            this.LblCadastroFuncionario = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.BtnSalvar = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.BtnNovo = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.BtnLimpar = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.BtnSair = ((System.Windows.Controls.Button)(target));
            return;
            case 7:
            this.TxtNome = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.TxtNumero = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.TxtComplemento = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.TxtBairro = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.TxtCidade = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.TxtUf = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.TxtCep = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.TxtDataAdmissao = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.TxtDataDemissao = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.TxtEndereco = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.LblNome = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.LblEndereco = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.LblNumero = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.LblComplemento = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.LblBairro = ((System.Windows.Controls.Label)(target));
            return;
            case 22:
            this.LblCidade = ((System.Windows.Controls.Label)(target));
            return;
            case 23:
            this.LbllUf = ((System.Windows.Controls.Label)(target));
            return;
            case 24:
            this.LblCep = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.LblDataAdmissão = ((System.Windows.Controls.Label)(target));
            return;
            case 26:
            this.LblDataDemissao = ((System.Windows.Controls.Label)(target));
            return;
            case 27:
            this.TxtEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 28:
            this.LblEmail = ((System.Windows.Controls.Label)(target));
            return;
            case 29:
            this.TxtLogin = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.TxtSenha = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.LblLogin = ((System.Windows.Controls.Label)(target));
            return;
            case 32:
            this.LblSenha = ((System.Windows.Controls.Label)(target));
            return;
            case 33:
            this.TxtCargo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 34:
            this.TxtTelefone = ((System.Windows.Controls.TextBox)(target));
            return;
            case 35:
            this.LblTelefone = ((System.Windows.Controls.Label)(target));
            return;
            case 36:
            this.TxtCelular = ((System.Windows.Controls.TextBox)(target));
            return;
            case 37:
            this.LblCelular = ((System.Windows.Controls.Label)(target));
            return;
            case 38:
            this.LblCargo = ((System.Windows.Controls.Label)(target));
            return;
            case 39:
            this.CmbEmpresa = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 40:
            this.LblEmpresa = ((System.Windows.Controls.Label)(target));
            return;
            case 41:
            this.TxtDataCadCliente = ((System.Windows.Controls.TextBox)(target));
            
            #line 352 "..\..\FrmCadFuncionario.xaml"
            this.TxtDataCadCliente.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtNome_KeyDown);
            
            #line default
            #line hidden
            return;
            case 42:
            this.LblDatCadCliente = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

