﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading; //progressBar

namespace ThuiTapiocaDesktop
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        int i = 0;
        string login, senha;

        public MainWindow()
        {
            InitializeComponent();
            TxtLogin.Focus();
        }
        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            var sair = MessageBox.Show("Tem certeza que deseja encerrar a aplicação?", "Encerrar aplicação", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (sair != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                Application.Current.Shutdown();
            }

        }

        private void TxtLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                login = TxtLogin.Text;
                PswSenha.IsEnabled = true;
                PswSenha.Focus();
            }
        }

        private void PswSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                senha = PswSenha.Password;
                BtnEntrar.IsEnabled = true;
                BtnRecuperarSenha.IsEnabled = true;
            }

        }
		//Ainda será alterado para se conectar diretamente com o banco
        private void BtnEntrar_Click(object sender, RoutedEventArgs e)
        {
            Thread.Sleep(10);//10 s
            PgbLogin.Value = 0;
            i = 0;
            Task.Run(() =>
            {

                while (i < 100)
                {
                    i++;
                    Thread.Sleep(50);
                    this.Dispatcher.Invoke(() => //usar para att imediata obrigatoria

                    {
                        PgbLogin.Value = i;

                        while (i == 100)
                        {
                            if (login == "ADMIN" && senha == "1234")
                            {
                                Hide();
                                FrmMenuPrincipal frm = new FrmMenuPrincipal();
                                frm.Show();
                                frm.LblLogado.Content = login;
                                break;
                            }
                            else if (login == "FUNC" && senha == "1010")
                            {
                                Hide();
                                FrmMenuPrincipal frm = new FrmMenuPrincipal();
                                frm.Show();
                                frm.LblLogado.Content = login;
                                frm.BtnFuncionario.IsEnabled = false;
                                frm.BtnCardapio.IsEnabled = false;
                                frm.BtnCliente.IsEnabled = false;
                                frm.BtnEmpresa.IsEnabled = false;
                                frm.BtnFornecedor.IsEnabled = false;

                            }

                            else if (login == "GERE" && senha == "1111")
                            {

                                FrmMenuPrincipal frm = new FrmMenuPrincipal();
                                frm.Show();
                                frm.LblLogado.Content = login;
                                frm.BtnFuncionario.IsEnabled = false;
                                frm.BtnFornecedor.IsEnabled = false;
                            }

                            else
                            {
                                MessageBox.Show("Favor preencher o login ou a senha corretamente!");
                                TxtLogin.Clear();
                                PswSenha.Clear();
                                TxtLogin.Focus();
                                PgbLogin.Value = 0;
                                PswSenha.IsEnabled = false;
                                BtnEntrar.IsEnabled = false;
                            }

                            break;
                        }
                    });
                }
            });
        }

//FIM do Login do usuario, pode estar sujeito a mudanças...//

        private void BtnRecuperarSenha_Click(object sender, RoutedEventArgs e)
        {

        }



    }
}
